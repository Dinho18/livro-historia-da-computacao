# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Surgimento da Televisão](https://gitlab.com/Dinho18/livro-historia-da-computacao/-/blob/master/Capitulos/Surgimento%20da%20Televis%C3%A3o.md)
1. [Surgimento do Computador](https://gitlab.com/Dinho18/livro-historia-da-computacao/-/blob/master/Capitulos/Surgimento%20do%20Computador.md)





## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11207872/avatar.png?width=400)  | Alanderson S. Lopes | Dinho | [alanderson@utfpr.edu.br](mailto:alanderson@utfpr.edu.br)

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11225121/avatar.png?width=400)  | Gisele Kamile Siqueira | Gih | [gihsiqueira02@gmail.com](mailto:gihsiqueira02@gmail.com)


