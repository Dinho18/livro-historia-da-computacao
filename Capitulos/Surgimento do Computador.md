# História do Computador 

O surgimento da palavra "Computador" vem do verbo "computar" que significa "calcular", dessa forma, uma das primeiras máquinas de computar foi o “ábaco”, instrumento mecânico de origem chinesa criado no século V a.C., assim sendo considerado o "primeiro Computador", uma espécie de calculadora que realizava operações algébricas.

Por volta de 1640, o matemático francês Pascal inventa a primeira máquina de calcular automática. Essa máquina foi sendo aperfeiçoada nas décadas seguintes até chegar no conceito que conhecemos hoje.

George Boole (1815-1864) foi um dos fundadores da lógica matemática. Essa nova área da matemática, se tornou uma poderosa ferramenta no projeto e estudo de circuitos eletrônicos e arquitetura de computadores.

Já no século XIX, o matemático inglês Charles Babbage criou uma máquina analítica que, a grosso modo, é comparada com o computador atual com memória e programas.

Através dessa invenção, alguns estudiosos o consideram o “Pai da Informática”.

Assim, as máquinas de computar foram cada vez mais incluindo a variedade de cálculos matemáticos (adição, subtração, divisão, multiplicação, raiz quadrada, logaritmos, etc).

Atualmente é possível encontrar máquinas de computar muito complexas.

### **Primeira Geração (1951-1959)**

Os computadores de primeira geração funcionavam por meio de circuitos e válvulas eletrônicas. Possuíam o uso restrito, além de serem imensos e consumirem muita energia.

Um exemplo é o ENIAC (Eletronic Numerical Integrator and Computer) que consumia cerca de 200 quilowatts e possuía 19.000 válvulas.

### **Terceira Geração (1965-1975)**

Os computadores da terceira geração funcionavam por circuitos integrados. Esses substituíram os transistores e já apresentavam uma dimensão menor e maior capacidade de processamento.

Foi nesse período que os chips foram criados e a utilização de computadores pessoais começou.

### **Quarta Geração (1975-até os dias atuais)**
Com o desenvolvimento da tecnologia da informação, os computadores diminuem de tamanho, aumentam a velocidade e capacidade de processamento de dados. São incluídos os microprocessadores com gasto cada vez menor de energia.

Nesse período, mais precisamente a partir da década de 90, há uma grande expansão dos computadores pessoais.

Além disso, surgem os softwares integrados e a partir da virada do milênio, começam a surgir os computadores de mão. Ou seja, os smartphones, iPod, iPad e tablets, que incluem conexão móvel com navegação na web.

Segundo a classificação acima, nós pertencemos à quarta geração dos computadores, o que tem revelado uma evolução incrível nos sistemas de informação.

Observe que antes a evolução dos computadores ocorria de maneira mais lenta. Com o desenvolvimento da sociedade podemos ver a evolução dessas máquinas em dias ou meses.

Alguns estudiosos preferem acrescentar a “Quinta Geração de Computadores” com o aparecimento dos supercomputadores, utilizados por grandes corporações como a NASA.

Nessa geração, é possível avaliar a evolução da tecnologia multimídia, da robótica e da internet.

# Referências:

1. DIANA, D. História e evolução dos Computadores. Disponível em: <https://www.todamateria.com.br/historia-e-evolucao-dos-computadores/>. Acesso em: 8 junho. 2022.
